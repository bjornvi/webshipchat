import './hello.html';
import { Tasks } from '../../../api/links/links.js';
import '../../../startup/accounts-config.js'
Template.hello.onRendered(function(){
  console.log('onrendered');
})

Template.hello.onCreated(function helloOnCreated() {
  
  //autorun opmaken zodat wanneer de const user veranderd deze methode word uitgevoerd
  const template = this;
  template.autorun(function(){
    //ophalen van de user voor te kijken welke user ik ben 
    const user = Meteor.user()
    var username = null;
    // console.log(user);
    try{
        username= user.username;
        // console.log("username:",username);
        
    }
    catch{
      console.log("username nog niet gekend            ");
    }

    if(username)
    //op basis van username kunnen we de juiste subscride opvragen
        {template.subscribe("messages.user",username);}
  
  })
  
  
 

});

//ophalen van de juiste messages
Template.hello.helpers({
  messages() {
    mes = Tasks.find({});
    // console.log(mes);
    return mes;
  },
});

//event voor het versturen van een message 
Template.hello.events({
  'submit .new-task'(event) {

    //ophalen van gegevens uit event
    user=  Meteor.user();
    const target = event.target;
    const TO= target.TO.value;
    const message = target.mes.value;
    
// console.log(user.username);

   
    // Prevent default browser form submit
      event.preventDefault();

      //zijn we Receiver of user (Role)
      if(user.username!= "Receiver")
      {
        //enkel zenden naar receiver dit zijn users
          if(TO=="Receiver")
          {
            Meteor.call('message.insert',TO,message);
          }
          else{
            console.log("mag niet naar de users sturen")
          }
      }
      else{
        Meteor.call('message.insert',TO,message);
      }
  }
}); 
   
  

