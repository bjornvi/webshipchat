// Methods related to links

import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Tasks } from './links.js';

Meteor.methods({




  'message.insert'(TO, message) {

    check(TO, String);
    check(message, String);
    // console.log("methods");

    return Tasks.insert({
      owner: Meteor.userId(),
      username: Meteor.user().username,
    TO,
    message,
    createdAt: new Date(),
    });
  },
});
