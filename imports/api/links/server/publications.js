// All links-related publications

import { Meteor } from 'meteor/meteor';
import { Tasks } from '../links.js';

Meteor.publish('messages.all', function () {
  return Tasks.find();
});

Meteor.publish('messages.user', function (username) {
  return Tasks.find({TO:username}, { sort: { createdAt: -1 },limit:10 });
});

